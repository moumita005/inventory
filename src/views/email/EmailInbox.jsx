/* eslint-disable prettier/prettier */
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import ErrorOutlineOutlinedIcon from '@mui/icons-material/ErrorOutlineOutlined';
import FolderOpenOutlinedIcon from '@mui/icons-material/FolderOpenOutlined';
import KeyboardArrowDownOutlinedIcon from '@mui/icons-material/KeyboardArrowDownOutlined';
import LabelOutlinedIcon from '@mui/icons-material/LabelOutlined';
import LocalOfferOutlinedIcon from '@mui/icons-material/LocalOfferOutlined';
import MoreVertOutlinedIcon from '@mui/icons-material/MoreVertOutlined';
import MoveToInboxOutlinedIcon from '@mui/icons-material/MoveToInboxOutlined';
import StarBorderOutlinedIcon from '@mui/icons-material/StarBorderOutlined';
import { Checkbox, Grid, Table, TableCell, TableContainer, TableRow, Typography } from '@mui/material';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Fade from '@mui/material/Fade';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { makeStyles } from '@mui/styles';
import * as React from 'react';
import './EmailInbox.css';

const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

const columns = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'firstName', headerName: 'First name', width: 130 },
    { field: 'lastName', headerName: 'Last name', width: 130 },
    {
        field: 'age',
        headerName: 'Age',
        type: 'number',
        width: 90
    },
    {
        field: 'fullName',
        headerName: 'Full name',
        description: 'This column has a value getter and is not sortable.',
        sortable: false,
        width: 160,
        valueGetter: (params) => `${params.getValue(params.id, 'firstName') || ''} ${params.getValue(params.id, 'lastName') || ''}`
    }
];

const rows = [
    { id: 1, lastName: 'Snow', firstName: 'Jon', age: 35 },
    { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
    { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
    { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16 },
    { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null },
    { id: 6, lastName: 'Melisandre', firstName: null, age: 150 },
    { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
    { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36 },
    { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65 }
];

const useStyles = makeStyles({
    btn: {
        color: '#fff'
        // bgcolor: '#2196f3'
    }
});

const EmailInbox = () => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <div
            style={{
                height: '100%',
                width: '100%',
                // border: "1px solid gray",
                padding: 20,
                borderRadius: '10px',
                backgroundColor: '#fff',
                boxShadow: '0 0 5px #e0e0e0'
            }}
        >
            <Grid container spacing={1} style={{ marginBottom: '10px' }}>
                <Grid item>
                    <ButtonGroup variant="contained" aria-label="outlined primary button group">
                        <Button
                            sx={{
                                color: '#fff'
                                // bgcolor: '#2196f3'
                            }}
                        >
                            <MoveToInboxOutlinedIcon />
                        </Button>
                        <Button
                            sx={{
                                color: '#fff'
                                // bgcolor: '#2196f3'
                            }}
                        >
                            <ErrorOutlineOutlinedIcon />
                        </Button>
                        <Button
                            sx={{
                                color: '#fff'
                                // bgcolor: '#2196f3'
                            }}
                        >
                            <DeleteOutlineOutlinedIcon />
                        </Button>
                    </ButtonGroup>
                </Grid>
                <Grid item>
                    <Button
                        sx={{
                            color: '#fff'
                            // bgcolor: '#2196f3'
                        }}
                        size="large"
                        variant="contained"
                        id="fade-button"
                        aria-controls="fade-menu"
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                        startIcon={<FolderOpenOutlinedIcon />}
                        endIcon={<KeyboardArrowDownOutlinedIcon />}
                        onClick={handleClick}
                    />
                    <Menu
                        id="fade-menu"
                        MenuListProps={{
                            'aria-labelledby': 'fade-button'
                        }}
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        TransitionComponent={Fade}
                    >
                        <MenuItem onClick={handleClose}>Profile</MenuItem>
                        <MenuItem onClick={handleClose}>My account</MenuItem>
                        <MenuItem onClick={handleClose}>Logout</MenuItem>
                    </Menu>
                </Grid>
                <Grid item>
                    <Button
                        sx={{
                            color: '#fff'
                            // bgcolor: '#2196f3'
                        }}
                        size="large"
                        variant="contained"
                        id="fade-button"
                        aria-controls="fade-menu"
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                        startIcon={<LocalOfferOutlinedIcon />}
                        endIcon={<KeyboardArrowDownOutlinedIcon />}
                        onClick={handleClick}
                    />
                    <Menu
                        id="fade-menu"
                        MenuListProps={{
                            'aria-labelledby': 'fade-button'
                        }}
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        TransitionComponent={Fade}
                    >
                        <MenuItem onClick={handleClose}>Profile</MenuItem>
                        <MenuItem onClick={handleClose}>My account</MenuItem>
                        <MenuItem onClick={handleClose}>Logout</MenuItem>
                    </Menu>
                </Grid>
                <Grid item>
                    <Button
                        sx={{
                            color: '#fff'
                            // bgcolor: '#2196f3'
                        }}
                        variant="contained"
                        id="fade-button"
                        aria-controls="fade-menu"
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                        endIcon={<MoreVertOutlinedIcon />}
                        onClick={handleClick}
                    >
                        More
                    </Button>
                    <Menu
                        id="fade-menu"
                        MenuListProps={{
                            'aria-labelledby': 'fade-button'
                        }}
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        TransitionComponent={Fade}
                    >
                        <MenuItem onClick={handleClose}>Profile</MenuItem>
                        <MenuItem onClick={handleClose}>My account</MenuItem>
                        <MenuItem onClick={handleClose}>Logout</MenuItem>
                    </Menu>
                </Grid>
            </Grid>
            {/* <DataGrid
        rows={rows}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        checkboxSelection
      /> */}
            <TableContainer>
                <Table>
                    <TableRow>
                        <TableCell>
                            <Checkbox {...label} />
                        </TableCell>
                        <TableCell>
                            <StarBorderOutlinedIcon />
                        </TableCell>
                        <TableCell>
                            <LabelOutlinedIcon />
                        </TableCell>
                        <TableCell>
                            <Typography>Peter, me (3)</Typography>
                        </TableCell>
                        <TableCell>
                            <Typography>
                                Hello – Trip home from Colombo has been arranged, then Jenna will come get me from Stockholm. :)
                            </Typography>
                        </TableCell>
                        <TableCell>
                            <Typography>Mar 6</Typography>
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>
                            <Checkbox {...label} />
                        </TableCell>
                        <TableCell>
                            <StarBorderOutlinedIcon />
                        </TableCell>
                        <TableCell>
                            <LabelOutlinedIcon />
                        </TableCell>
                        <TableCell>
                            <Typography>Peter, me (3)</Typography>
                        </TableCell>
                        <TableCell>
                            <Typography>
                                Hello – Trip home from Colombo has been arranged, then Jenna will come get me from Stockholm. :)
                            </Typography>
                        </TableCell>
                        <TableCell>
                            <Typography>Mar 6</Typography>
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>
                            <Checkbox {...label} />
                        </TableCell>
                        <TableCell>
                            <StarBorderOutlinedIcon />
                        </TableCell>
                        <TableCell>
                            <LabelOutlinedIcon />
                        </TableCell>
                        <TableCell>
                            <Typography>Peter, me (3)</Typography>
                        </TableCell>
                        <TableCell>
                            <Typography>
                                Hello – Trip home from Colombo has been arranged, then Jenna will come get me from Stockholm. :)
                            </Typography>
                        </TableCell>
                        <TableCell>
                            <Typography>Mar 6</Typography>
                        </TableCell>
                    </TableRow>
                </Table>
            </TableContainer>
        </div>
    );
};

export default EmailInbox;
