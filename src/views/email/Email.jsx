import { Grid } from '@mui/material';
import React from 'react';
import EmailDetail from './EmailDetail';
// import Breadcrumb from '../../components/Breadcrumb/Breadcrumb';
// import EmailInbox from './EmailInbox';
import EmailSidebar from './EmailSidebar';

// const styles = {
//   backgroundColor: "white",
//   // padding: "20px",
//   borderRadius: "10px",
//   height: "80vh",
// };

const Email = () => (
    <Grid container spacing={4}>
        {/* <Grid item xs={12} sm={12} md={12} lg={12}>
            <Breadcrumb />
        </Grid> */}
        <Grid item xs={12} sm={5} md={4} lg={3}>
            <EmailSidebar />
        </Grid>
        <Grid item xs={12} sm={7} md={8} lg={9}>
            {/* <EmailInbox /> */}
            <EmailDetail />
        </Grid>
    </Grid>
);

export default Email;
