// import FavoriteIcon from "@mui/icons-material/Favorite";
// import ShareIcon from "@mui/icons-material/Share";
// import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
// import CardMedia from "@mui/material/CardMedia";
// import CardContent from "@mui/material/CardContent";
// import CardActions from "@mui/material/CardActions";
// import Collapse from "@mui/material/Collapse";
import ArrowBackIosOutlinedIcon from '@mui/icons-material/ArrowBackIosOutlined';
import ForwardTwoToneIcon from '@mui/icons-material/ForwardTwoTone';
import LabelOutlinedIcon from '@mui/icons-material/LabelOutlined';
import MoreHorizOutlinedIcon from '@mui/icons-material/MoreHorizOutlined';
import ReplyOutlinedIcon from '@mui/icons-material/ReplyOutlined';
import ReportGmailerrorredOutlinedIcon from '@mui/icons-material/ReportGmailerrorredOutlined';
import StarBorderOutlinedIcon from '@mui/icons-material/StarBorderOutlined';
import { CardActions, CardContent, Grid, IconButton } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import * as React from 'react';

const EmailDetail = () => (
    <Card style={{ padding: 15, boxShadow: '0 0 5px #e0e0e0' }}>
        {/* <CardHeader
        avatar={
          <Grid container spacing={2}>
            <Grid item>
              <IconButton aria-label="delete">
                <DeleteIcon />
              </IconButton>
            </Grid>
            <Grid item>
              <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                R
              </Avatar>
            </Grid>
          </Grid>
        }
        action={
          <IconButton aria-label="settings">
            <Typography variant="body2">9 sep</Typography>
          </IconButton>
        }
        title="Shrimp and Chorizo Paella"
        subheader="September 14, 2016"
      /> */}
        <CardContent>
            <Grid container justifyContent="flex-start" alignItems="">
                <Grid item xs={1} sm={1} md={1} lg={1} justifyContent="flex-start">
                    <Stack
                        direction="row"
                        // justifyContent="center"
                        alignItems="center"
                        spacing={1}
                    >
                        <IconButton>
                            <ArrowBackIosOutlinedIcon />
                        </IconButton>
                        <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
                    </Stack>
                </Grid>
                <Grid item xs={10} sm={10} md={10} lg={10} style={{ paddingLeft: 15 }}>
                    <Typography>Esther Garcia</Typography>
                    <Typography>Esther Garcia</Typography>
                </Grid>
                <Grid item xs={1} sm={1} md={1} lg={1}>
                    <Typography>9 sep</Typography>
                </Grid>
            </Grid>
        </CardContent>
        <CardContent>
            <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h6">Lorem ipsum dolor sit amet consectetur</Typography>
                </Grid>
                <Grid item>
                    <IconButton>
                        <StarBorderOutlinedIcon />
                    </IconButton>
                    <IconButton>
                        <LabelOutlinedIcon />
                    </IconButton>
                    <IconButton>
                        <ReportGmailerrorredOutlinedIcon />
                    </IconButton>
                    <IconButton>
                        <MoreHorizOutlinedIcon />
                    </IconButton>
                </Grid>
            </Grid>
        </CardContent>
        <CardContent>
            <Grid container direction="column" spacing={4}>
                <Grid item>
                    <Typography variant="body2">Lorem ipsum dolor sit amet.</Typography>
                </Grid>
                <Grid item>
                    <Typography variant="body2">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum nisi totam vitae dolorum nobis, quaerat facere ullam
                        odio molestias facilis. Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque optio nisi quod? Ipsam, ipsum
                        sequi asperiores enim doloribus obcaecati dolore libero quas sapiente deserunt iste illum minus quod tenetur et!
                    </Typography>
                </Grid>
                <Grid item>
                    <Typography variant="body2">
                        Lorem, ipsum.
                        <br /> Lorem, ipsum.
                    </Typography>
                </Grid>
            </Grid>
        </CardContent>
        <CardActions>
            <Stack direction="row" spacing={2}>
                <Button style={{ color: '#2196f3' }} variant="outlined" startIcon={<ReplyOutlinedIcon />}>
                    Reply
                </Button>
                <Button style={{ color: '#2196f3' }} variant="outlined" startIcon={<ForwardTwoToneIcon />}>
                    Forword
                </Button>
            </Stack>
        </CardActions>
    </Card>
);

export default EmailDetail;
