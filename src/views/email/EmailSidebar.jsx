import ControlPointIcon from '@mui/icons-material/ControlPoint';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import ImportantDevicesOutlinedIcon from '@mui/icons-material/ImportantDevicesOutlined';
import InsertDriveFileOutlinedIcon from '@mui/icons-material/InsertDriveFileOutlined';
import MailOutlinedIcon from '@mui/icons-material/MailOutlined';
import MarkEmailReadOutlinedIcon from '@mui/icons-material/MarkEmailReadOutlined';
import StarBorderPurple500OutlinedIcon from '@mui/icons-material/StarBorderPurple500Outlined';
import { Box, IconButton } from '@mui/material';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { makeStyles } from '@mui/styles';
import React from 'react';
// import CustomAvatar from '../../../components/Avatar/CustomAvatar';
import './EmailSidebar.css';

const useStyles = makeStyles({
    box: {
        border: '1px black'
    }
});

const EmailSidebar = () => {
    const classes = useStyles();
    const [selectedIndex, setSelectedIndex] = React.useState(1);

    const handleListItemClick = (event, index) => {
        setSelectedIndex(index);
    };

    return (
        <Box
            style={{
                padding: 25,
                boxShadow: '0 0 5px #e0e0e0',
                borderRadius: '10px',
                backgroundColor: '#fff'
            }}
        >
            <Button
                startIcon={<ControlPointIcon />}
                sx={{
                    color: '#fff'
                    // bgcolor: '#2196f3'
                }}
                size="large"
                fullWidth
                variant="contained"
            >
                Contained
            </Button>

            <nav aria-label="main mailbox folders">
                <List>
                    <ListItemButton
                        selected={selectedIndex === 0}
                        onClick={(event) => handleListItemClick(event, 0)}
                        secondaryAction={<IconButton edge="end" aria-label="delete" />}
                    >
                        <ListItemIcon>
                            <MailOutlinedIcon />
                        </ListItemIcon>
                        <ListItemText primary="Inbox" />
                    </ListItemButton>
                    <ListItemButton selected={selectedIndex === 1} onClick={(event) => handleListItemClick(event, 1)}>
                        <ListItemIcon>
                            <StarBorderPurple500OutlinedIcon />
                        </ListItemIcon>
                        <ListItemText primary="Starred" />
                    </ListItemButton>

                    <ListItemButton selected={selectedIndex === 2} onClick={(event) => handleListItemClick(event, 2)}>
                        <ListItemIcon>
                            <ImportantDevicesOutlinedIcon />
                        </ListItemIcon>
                        <ListItemText primary="Important" />
                    </ListItemButton>

                    <ListItemButton selected={selectedIndex === 3} onClick={(event) => handleListItemClick(event, 3)}>
                        <ListItemIcon>
                            <InsertDriveFileOutlinedIcon />
                        </ListItemIcon>
                        <ListItemText primary="Drafts" />
                    </ListItemButton>

                    <ListItemButton selected={selectedIndex === 4} onClick={(event) => handleListItemClick(event, 4)}>
                        <ListItemIcon>
                            <DeleteOutlineOutlinedIcon />
                        </ListItemIcon>
                        <ListItemText primary="Drafts" />
                    </ListItemButton>

                    <ListItemButton selected={selectedIndex === 5} onClick={(event) => handleListItemClick(event, 5)}>
                        <ListItemIcon>
                            <MarkEmailReadOutlinedIcon />
                        </ListItemIcon>
                        <ListItemText primary="Drafts" />
                    </ListItemButton>
                </List>
            </nav>

            <Divider />
            <nav aria-label="secondary mailbox folders">
                <List component="nav" aria-label="secondary mailbox folder">
                    <ListItemButton selected={selectedIndex === 6} onClick={(event) => handleListItemClick(event, 6)}>
                        <ListItemText primary="Trash" />
                    </ListItemButton>
                    <ListItemButton selected={selectedIndex === 7} onClick={(event) => handleListItemClick(event, 7)}>
                        <ListItemText primary="Spam" />
                    </ListItemButton>
                </List>
            </nav>
        </Box>
    );
};

export default EmailSidebar;
