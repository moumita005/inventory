/* eslint-disable prettier/prettier */
// project imports
import MainLayout from 'layout/MainLayout';
import { lazy } from 'react';
import Loadable from 'ui-component/Loadable';


// dashboard routing
const DashboardDefault = Loadable(lazy(() => import('views/dashboard/Default')));

// utilities routing
const UtilsTypography = Loadable(lazy(() => import('views/utilities/Typography')));
const UtilsColor = Loadable(lazy(() => import('views/utilities/Color')));
const UtilsShadow = Loadable(lazy(() => import('views/utilities/Shadow')));
const UtilsMaterialIcons = Loadable(lazy(() => import('views/utilities/MaterialIcons')));
const UtilsTablerIcons = Loadable(lazy(() => import('views/utilities/TablerIcons')));

// sample page routing
const SamplePage = Loadable(lazy(() => import('views/sample-page')));

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
    path: '/',
    element: <MainLayout />,
    children: [
        {
            path: '/',
            element: <DashboardDefault />
        },
        {
            path: '/dashboard/default',
            element: <DashboardDefault />
        },
        {
            path: '/utils/util-typography',
            element: <UtilsTypography />
        },
        {
            path: '/utils/util-color',
            element: <UtilsColor />
        },
        {
            path: '/utils/util-shadow',
            element: <UtilsShadow />
        },
        {
            path: '/icons/tabler-icons',
            element: <UtilsTablerIcons />
        },
        {
            path: '/icons/material-icons',
            element: <UtilsMaterialIcons />
        },
        {
            path: '/sell/add-new-invoice',
            element: <SamplePage />
        },
        {
            path: '/sell/manage-invoice',
            element: <SamplePage />
        },
        {
            path: '/sample-page',
            element: <SamplePage />
        },
        {
            path: '/customer/add-customer',
            element: <SamplePage />
        },
        {
            path: '/customer/manage-customer',
            element: <SamplePage />
        },
        {
            path: '/customer/credit-customer',
            element: <SamplePage />
        },
        {
            path: '//customer/paid-customer',
            element: <SamplePage />
        },
        {
            path: '/supplier/add-supplier',
            element: <SamplePage />
        },
        {
            path: '/supplier/manage-supplier',
            element: <SamplePage />
        },
        {
            path: '/supplier/supplier-payment',
            element: <SamplePage />
        },
        {
            path: '/supplier/supplier-ledger',
            element: <SamplePage />
        },
        
        // {
        //     path: '/icons/tabler-icons',
        //     element: <UtilsTablerIcons />
        // },
        // {
        //     path: '/icons/material-icons',
        //     element: <UtilsMaterialIcons />
        // },
    ]
};

export default MainRoutes;
