/* eslint-disable prettier/prettier */
// assets
import PeopleOutlinedIcon from '@mui/icons-material/PeopleOutlined';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import SellOutlinedIcon from '@mui/icons-material/SellOutlined';
import { IconKey } from '@tabler/icons';

// constant
const icons = {
    IconKey
};

// ==============================|| EXTRA PAGES MENU ITEMS ||============================== //

const pages = {
    id: 'pages',
    title: 'Pages',
    // caption: 'Pages Caption',
    type: 'group',
    children: [
        {
            id: 'sell',
            title: 'Sell',
            type: 'collapse',
            icon: SellOutlinedIcon,
            children: [
                {
                    id: 'add-new-invoice',
                    title: 'Add New Invoice',
                    type: 'item',
                    url: '/sell/add-new-invoice',
                    breadcrumbs: true
                },
                {
                    id: 'manage-invoice',
                    title: 'Manage Invoice',
                    type: 'item',
                    url: '/sell/manage-invoice',
                    breadcrumbs: true
                }
            ]
        },
        {
            id: 'customer',
            title: 'Customer',
            type: 'collapse',
            icon: PeopleOutlinedIcon,
            children: [
                {
                    id: 'add-customer',
                    title: 'Add Customer',
                    type: 'item',
                    url: '/customer/add-customer',
                    breadcrumbs: true
                },
                {
                    id: 'manage-customer',
                    title: 'Manage Customer',
                    type: 'item',
                    url: '/customer/manage-customer',
                    breadcrumbs: true
                },
                {
                    id: 'credit-customer',
                    title: 'Credit Customer',
                    type: 'item',
                    url: '/customer/credit-customer',
                    breadcrumbs: true
                },
                {
                    id: 'paid-customer',
                    title: 'Paid Customer',
                    type: 'item',
                    url: '/customer/paid-customer',
                    breadcrumbs: true
                }
            ]
        },
        {
            id: 'supplier',
            title: 'Supplier',
            type: 'collapse',
            icon: PersonOutlineOutlinedIcon,
            children: [
                {
                    id: 'add-supplier',
                    title: 'Add Supplier',
                    type: 'item',
                    url: '/supplier/add-supplier',
                    breadcrumbs: true
                },
                {
                    id: 'manage-supplier',
                    title: 'Manage Supplier',
                    type: 'item',
                    url: '/supplier/manage-supplier',
                    breadcrumbs: true
                },
                {
                    id: 'supplier-payment',
                    title: 'Supplier Payment',
                    type: 'item',
                    url: '/supplier/supplier-payment',
                    breadcrumbs: true
                },
                {
                    id: 'supplier-ledger',
                    title: 'Supplier Ledger',
                    type: 'item',
                    url: '/supplier/supplier-ledger',
                    breadcrumbs: true
                }
            ]
        },

        // {
        //     id: 'authentication',
        //     title: 'Authentication',
        //     type: 'collapse',
        //     icon: icons.IconKey,

        //     children: [
        //         {
        //             id: 'login3',
        //             title: 'Login',
        //             type: 'item',
        //             url: '/pages/login/login3',
        //             target: true
        //         },
        //         {
        //             id: 'register3',
        //             title: 'Register',
        //             type: 'item',
        //             url: '/pages/register/register3',
        //             target: true
        //         }
        //     ]
        // }
    ]
};

export default pages;
