/* eslint-disable prettier/prettier */
// material-ui
import { ButtonBase } from '@mui/material';
// project imports
import config from 'config';
import { Link } from 'react-router-dom';
import logo from '../../../assets/images/logo/logo.png';


// import Logo from 'ui-component/Logo';


// ==============================|| MAIN LOGO ||============================== //

const LogoSection = () => (
    <ButtonBase disableRipple component={Link} to={config.defaultPath}>
        {/* <Logo /> */}
        <img style={{height: 30, width: 150}} src={logo} alt="logo" />
    </ButtonBase>
);

export default LogoSection;
