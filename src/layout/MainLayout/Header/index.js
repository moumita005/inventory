/* eslint-disable prettier/prettier */
import { Avatar, Box, ButtonBase } from '@mui/material';
// material-ui
import { useTheme } from '@mui/material/styles';
// assets
import { IconMenu2 } from '@tabler/icons';
import Customization from 'layout/Customization';
import PropTypes from 'prop-types';
// project imports
import LogoSection from '../LogoSection';
// import Others from './MobileView';
import NotificationSection from './NotificationSection';
import ProfileSection from './ProfileSection';
import SearchSection from './SearchSection';

// ==============================|| MAIN NAVBAR / HEADER ||============================== //

const Header = ({ handleLeftDrawerToggle }) => {
    const theme = useTheme();

    return (
        <>
            {/* logo & toggler button */}
            {/* <Box style={{display: "flex", flexDirection: "column"}}>
            <Box style={{padding: "0 10px 15px 10px", display: "flex", justifyContent: "center"}} component="span" sx={{ display: { sm: "none", md: 'none', lg: 'none' }, flexGrow: 1 }}>
                    <LogoSection />
                </Box> */}
            {/* <Box style={{display: "flex", alignItems: "center"}}> */}
            <Box
                sx={{
                    width: 228,
                    display: 'flex',
                    [theme.breakpoints.down('md')]: {
                        width: 'auto'
                    }
                }}
            >
                <Box component="span" sx={{ display: { xs: 'none', md: 'flex', lg: "flex" }, justifyContent: "space-around", flexGrow: 1 , }}>
                    <LogoSection />
                </Box>
                <ButtonBase sx={{ borderRadius: '12px', overflow: 'hidden',  display: { xs: 'contents', md: 'none', lg: "none"} }}>
                    <Avatar
                        variant="rounded"
                        sx={{
                            ...theme.typography.commonAvatar,
                            ...theme.typography.mediumAvatar,
                            transition: 'all .2s ease-in-out',
                            // background: theme.palette.secondary.light,
                            background: {
                                xs: 'none',
                                sm: theme.palette.secondary.light,
                                md: theme.palette.secondary.light,
                                lg: theme.palette.secondary.light
                            },
                            color: theme.palette.secondary.dark,
                            '&:hover': {
                                // background: theme.palette.secondary.dark,
                                background: {
                                    xs: 'none',
                                    sm: theme.palette.secondary.light,
                                    md: theme.palette.secondary.light,
                                    lg: theme.palette.secondary.light
                                },
                                color: theme.palette.secondary.dark
                            }
                        }}
                        onClick={handleLeftDrawerToggle}
                        color="inherit"
                    >
                        <IconMenu2 stroke={1.5} size="1.3rem" />
                    </Avatar>
                </ButtonBase>
            </Box>

            {/* header search */}
            <Box sx={{ display: { xs: 'block', md: 'none', lg: 'none' }, marginLeft: '12px' }}>
                <LogoSection />
            </Box>

            <Box sx={{ display: { xs: 'none', md: 'contents', lg: 'contents' } }}>
            <ButtonBase sx={{ borderRadius: '12px', overflow: 'hidden', marginLeft: "8px" }}>
                    <Avatar
                        variant="rounded"
                        sx={{
                            ...theme.typography.commonAvatar,
                            ...theme.typography.mediumAvatar,
                            transition: 'all .2s ease-in-out',
                            // background: theme.palette.secondary.light,
                            background: {
                                xs: 'none',
                                sm: theme.palette.secondary.light,
                                md: theme.palette.secondary.light,
                                lg: theme.palette.secondary.light
                            },
                            color: theme.palette.secondary.dark,
                            '&:hover': {
                                // background: theme.palette.secondary.dark,
                                background: {
                                    xs: 'none',
                                    sm: theme.palette.secondary.light,
                                    md: theme.palette.secondary.light,
                                    lg: theme.palette.secondary.light
                                },
                                color: theme.palette.secondary.dark
                            }
                        }}
                        onClick={handleLeftDrawerToggle}
                        color="inherit"
                    >
                        <IconMenu2 stroke={1.5} size="1.3rem" />
                    </Avatar>
                </ButtonBase>
                <SearchSection />
            </Box>
            <Box sx={{ flexGrow: 1 }} />
            <Box sx={{ flexGrow: 1 }} />

            {/* notification & profile */}
            <Box sx={{ display: { xs: 'none', md: 'contents', lg: 'contents' } }}>
                <NotificationSection />
                <ProfileSection />
                <Customization />
            </Box>
            <Box sx={{ display: { xs: 'contents', md: 'none', lg: 'none' } }}>
                <SearchSection />
                <NotificationSection />
                <ProfileSection />
                {/* <Others /> */}
            </Box>
            {/* </Box> */}
            {/* </Box> */}
        </>
    );
};

Header.propTypes = {
    handleLeftDrawerToggle: PropTypes.func
};

export default Header;
