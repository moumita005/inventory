import FavoriteIcon from '@mui/icons-material/Favorite';
import FolderIcon from '@mui/icons-material/Folder';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import RestoreIcon from '@mui/icons-material/Restore';
import { BottomNavigationAction } from '@mui/material';
import BottomNavigation from '@mui/material/BottomNavigation';
import Paper from '@mui/material/Paper';
import * as React from 'react';

const BottomNav = () => {
    const [value, setValue] = React.useState('recents');

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <>
            <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }} elevation={3}>
                <BottomNavigation
                    showLabels
                    value={value}
                    onChange={(event, newValue) => {
                        setValue(newValue);
                    }}
                >
                    <BottomNavigationAction label="Recents" value="recents" icon={<RestoreIcon />} />
                    <BottomNavigationAction label="Favorites" value="favorites" icon={<FavoriteIcon />} />
                    <BottomNavigationAction label="Nearby" value="nearby" icon={<LocationOnIcon />} />
                    <BottomNavigationAction label="Folder" value="folder" icon={<FolderIcon />} />
                    {/* <Grid style={{ padding: '15px 10px' }} alignItems="center" container spacing={2}>
                    <Grid item xs={4}>
                        <SearchSection />
                    </Grid>
                    <Grid item xs={4}>
                        <NotificationSection />
                    </Grid>
                    <Grid item xs={4}>
                        <Customization />
                    </Grid>
                </Grid> */}
                </BottomNavigation>
            </Paper>
        </>
    );
};

export default BottomNav;
